console.log("Hellow world");
// [SECTION] Functions
// Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked.
// They are also used to prevent repeating lines/blocks of codes that perform the same task/function

// Function declarations
// Function statement defines a function with the specified parameters
/*
Syntax:
    function functionName(){
        code block(statement)
    }

    function keyword - use to define a javascript function 
    
    functionName - the function name is used so we are able to used so we are able to call/invoke are declared function

    function code block({}) - the statements which comprise the body of the function. This is where the code to be executed



*/

function printName(){
    console.log("My name is John");
}

// [SECTION] Function Invocation
    // It is common to use the term "call a function" instead fo "invoke a function"
    // Let's invoke / call the function that was declared.


printName();

 declaredFunction(); //- Results in an error. much like variables, we cannot invoke a function that we have not define yet.


// declared functions can be hoisted. As long as the function has been defined
// hoisting is javascript's behavior for certain variables(var) and functions to run or use them for declaration.
function declaredFunction(){
    console.log("Hellow World");
}

declaredFunction();

// [SECTION] Function Declaration vs expression
// Function Declaration
// A function can be created through function declaration by using the function keywoard and adding a function name


// Function Expression
    // A function can be also stored in a variable. This is called as function expression.
    // a Funcion expression is an anonymous function assigned to the variable function
    //  anonymous function - a function without a name

    
    let variableFunction = function(){
        console.log("Hello Again!");
    }

    variableFunction();

    let funcExpression = function funcName(){
        console.log("Hello from the other side");
    }

    // funcExpression = declaredFunction();



    declaredFunction();
    // Reassigning a declare function and expression.
    declaredFunction = function(){
        console.log("updated declaredFunction");
    }

    declaredFunction();

    // funcExpression();

    // funcName();

    functionExpression = function(){
        console.log("updated funcExpression");
    }

    funcExpression();

    const constantFunc =function(){
        console.log("Initialized with const");
    }



    constantFunc();

    //constantFunc = function(){
    //    console.log("New value");
    //}

     //constantFunc();

    // [SECTION] Function Scooping
    /*
    Scope is the accessibility / visibility of a variables in the code.
    
    Javascript Variables has 3 types of scope:
    1. local / block scope
    2. global scope
    3. function scope

    */

{
    let localVar = "Armando Perez";
    console.log(localVar);
}

let globalVar = "Mr Worldwide";

console.log(globalVar);
//console.log(localVar); //result in error


// Function Scopes
// Javascript has function scopes: Each function creates a new scope
// Variables defined inside a function are not accessible / visible outside the function
// Variables declared with var, let, and const are quite similar when declared inside a function.


function showNames(){
    // Function scoped variables;
    var functionVar = "Joe";
    const functionConst = "John";
    let functionLet = "Jane";

    console.log(functionVar);
    console.log(functionConst);
    console.log(functionLet);
}

showNames();
// Error - This are function scoped variable and cannot be accessed outside the function they were declared
// console.log(functionVar);
// console.log(functionConst);
// console.log(functionLet);


// Nested Functions
//  You can create another function inside a function
// This is called a nested function

function myNewFunction(){
    let name = "Jane";


function nestedFunction(){
    let nestedName = "John";
    console.log(name);
}
    //console.log(nestedName); //result to not defined error
}

myNewFunction();
// nestedFunction(); // result to not defined error




// Function and Global Scope variables

// Global Scoped Variable
let globalName = "Alexandro";

function myNewFunction2(){
    let nameInside = "Renz";
    console.log(globalName);
}

myNewFunction2();

console.log(nameInside);